package org.theorcastudios.controllers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.sun.tools.javac.util.BasicDiagnosticFormatter;
import org.apache.log4j.BasicConfigurator;
import org.theorcastudios.main.models.Messages;
import java.sql.SQLException;


public class Main {

    private static void connectDB(){

        try {


            Class.forName("org.h2.Driver");

            String url = "jdbc:h2:tcp://localhost/islack-bot";
            String user = "Fortunat";
            String pwds = null;

            // this uses h2 by default but change to match your database
            // create a connection source to our database
            ConnectionSource connectionSource = new JdbcConnectionSource(url);

            // instantiate the dao
            Dao<Messages, String> messagesStringDao =  DaoManager.createDao(connectionSource, Messages.class);



        } catch (Exception e){

            e.getStackTrace();

        }
    }

    public static void main(String[] args) {
        connectDB();

        Messages messages = new Messages();

        messages.setSent("Hello");
        messages.setDate_sent(null);
        messages.setResponse("okay");
        messages.setId(20);
        System.out.println(messages.getResponse());


    }
}
