
/**
 * Created by fotty on 2/23/16.
 */

package org.theorcastudios.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.types.DateType;
import com.j256.ormlite.table.DatabaseTable;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.sql.Date;


@DatabaseTable(tableName = "message")
public class Messages {

    //DB fields
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String sent;

    @DatabaseField(canBeNull = false)
    private Date date_sent;

    @DatabaseField
    private String response;

    //no argument constructor
    public Messages() {

    }

    public Messages(String sent, String response) {
        this.sent = sent;
        this.response = response;
        this.date_sent = new Date(System.currentTimeMillis());
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Messages{" +
                "id=" + id +
                ", sent='" + sent + '\'' +
                ", date_sent=" + date_sent +
                ", response='" + response + '\'' +
                '}';
    }
}
